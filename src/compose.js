const compose =
  (...validators) =>
  (value) =>
    validators.every((validator) => validator(value));

module.exports = compose;
