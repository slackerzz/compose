const compose = require("./compose");

const isString = (value) => value && typeof value === "string";
const isNumber = (value) => value && typeof value === "number";
const ofLength = (length) => (value) => value && value.length === length;
const biggerThan = (number) => (value) => value > number;
const smallerThan = (number) => (value) => value < number;
const isFiveLetter = compose(isString, ofLength(5));
const isTeenager = compose(isNumber, biggerThan(12), smallerThan(20));

module.exports = {
  isString,
  isNumber,
  ofLength,
  biggerThan,
  smallerThan,
  isFiveLetter,
  isTeenager,
};
