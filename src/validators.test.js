const { isFiveLetter, isTeenager } = require("./validators");

describe("isFiveLetter function test", () => {
  it("should return true", () => {
    expect(isFiveLetter("water")).toBe(true);
  });
  it("should return true", () => {
    expect(isFiveLetter("focus")).toBe(true);
  });
  it("should return false", () => {
    expect(isFiveLetter("too long")).toBe(false);
  });
});

describe("isTeenager function test", () => {
  it("should return true", () => {
    expect(isTeenager(13)).toBe(true);
  });
  it("should return true", () => {
    expect(isTeenager(18)).toBe(true);
  });
  it("should return false", () => {
    expect(isTeenager(2)).toBe(false);
  });
});
